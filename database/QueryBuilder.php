<?php

class QueryBuilder {
    public $pdo;

    public function __construct() {
        $this->pdo = new PDO('mysql:host=localhost; dbname=test', 'root','polina351');
    }

    public function all($table) {
        $sql = "SELECT * FROM $table";
        $statement = $this->pdo->prepare($sql);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function store($table, $data) {
        $keys = array_keys($data);
        $stringOfKeys = implode(',', $keys);
        $placeHolders = ':' . implode(', :', $keys);

        $sql = "INSERT INTO $table ($stringOfKeys) VALUES ($placeHolders)";
        $statement = $this->pdo->prepare($sql);
        $statement->execute($_POST);
    }

    public function getOne($table, $id) {
        $statement = $this->pdo->prepare("SELECT * FROM $table WHERE id=:id");
        $statement->bindParam(':id', $id);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public function update($table, $data) {
        $fields = '';
        foreach($data as $key => $value) {
            $fields .= $key . '=:' . $key . ',';
        }
        $field = rtrim($fields, ',');

        $sql = "UPDATE $table SET $field WHERE id=:id";
        $statement = $this->pdo->prepare($sql);
        $statement->execute($data);
    }

    public function delete($table, $id) {
        $id = $_GET['id'];
        $sql = "DELETE FROM $table WHERE id=$id";
        $statement = $this->pdo->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
    }
}