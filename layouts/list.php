<?php
require '../database/QueryBuilder.php';

$db = new QueryBuilder();
$tasks = $db->all('tasks');
?>
<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>All Tasks</h1>
            <a href="create.php" class="btn btn-success">Add Task</a>
            <table class="table">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Content</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($tasks as $task): ?>
                    <tr>
                        <td><?= $task['title']; ?></td>
                        <td><?= $task['meta_description']; ?></td>
                        <td><?= $task['content']; ?></td>
                        <td>
                            <a href="show.php?id=<?= $task['id']; ?>" class="btn btn-info">
                                Show
                            </a>
                            <a href="edit.php?id=<?= $task['id']; ?>" class="btn btn-warning">
                                Edit
                            </a>
                            <a onclick="return confirm('Are you sure?');" href="../crud/delete.php?id=<?= $task['id']; ?>" class="btn btn-danger">
                                Delete
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>